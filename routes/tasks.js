const express = require('express');
const Task = require('../models/task');

const router = express.Router();

// Get all tasks
router.get('/', async (req, res) => {
  try {
    const tasks = await Task.find();
    res.json(tasks);
  } catch (error) {
    res.status(500).json({ error: 'Server error' });
  }
});

router.post('/', async (req, res) => {
    try {
      const task = new Task(req.body);
      await task.save();
      res.status(201).json(task.toObject({ getters: true }));
    } catch (error) {
      res.status(500).json({ error: 'Server error' });
    }
  });
module.exports = router;