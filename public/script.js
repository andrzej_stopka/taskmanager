const taskForm = document.getElementById('taskForm');
const taskList = document.getElementById('taskList');

// Get all tasks
async function getTasks() {
  try {
    const response = await fetch('/tasks');
    const tasks = await response.json();
    taskList.innerHTML = '';
    tasks.forEach(task => {
      const li = document.createElement('li');
      li.textContent = `${task.title} - ${task.description}`;
      taskList.appendChild(li);
    });
  } catch (error) {
    console.error('Error:', error);
  }
}

// Create a new task
async function createTask(title, description) {
  try {
    const response = await fetch('/tasks', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ title, description, completed: false })
    });
    const task = await response.json();
    console.log('Task created:', task);
    getTasks();
  } catch (error) {
    console.error('Error:', error);
  }
}

// Form submit event
taskForm.addEventListener('submit', event => {
  event.preventDefault();
  const title = document.getElementById('title').value;
  const description = document.getElementById('description').value;
  createTask(title, description);
  taskForm.reset();
});

// Get tasks on page load
getTasks();