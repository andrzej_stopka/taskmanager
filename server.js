const express = require('express');
const mongoose = require('mongoose');
const dotenv = require('dotenv');

dotenv.config();

const app = express();
const port = 3000;


// Connect to MongoDB
mongoose.connect('mongodb://localhost/taskManager', {
  authSource: "admin",
  user: process.env.MONGODB_USER,
  pass: process.env.MONGODB_PASSWORD,
});

// Middleware
app.use(express.json());
app.use(express.static('public'));

const tasksRouter = require('./routes/tasks');
app.use('/tasks', tasksRouter);

// Start the server
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});